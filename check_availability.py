#!/usr/bin/env python3
# -*- coding: utf-8 -*- 

import argparse
import json
import logging
import sys
from datetime import datetime, timedelta

import requests
from fake_useragent import UserAgent

headers = {"User-Agent": UserAgent().random}

# URL = "https://www.recreation.gov/api/ticket/availability/facility/300015/monthlyAvailabilitySummaryView?year=2020&month=10&inventoryBucket=FIT"
URL = "https://www.recreation.gov/api/ticket/availability/facility/300015/monthlyAvailabilitySummaryView"
START_DATE = '2020-10-03'
YEAR = '2020'
MONTH = '10'

def generate_params():
	params = {"year": YEAR, "month": MONTH, "inventoryBucket": "FIT"}
	return params


def send_request():
	params = generate_params()
	resp = requests.get(URL, params=params, headers=headers)
	if resp.status_code != 200:
		raise RuntimeError(
			"failedRequest",
			"ERROR, {} code received from {}: {}".format(
				resp.status_code, URL, resp.text
			),
		)
	return resp.json()

resp = send_request()

payload = resp["facility_availability_summary_view_by_local_date"][START_DATE]
# print("Payload is %s" % (payload))

resv1 = resp["facility_availability_summary_view_by_local_date"][START_DATE]['tour_availability_summary_view_by_tour_id']['3000']['reservable']
# print("Reservable(1) is %s" % (resv1))

resv2 = resp["facility_availability_summary_view_by_local_date"][START_DATE]['tour_availability_summary_view_by_tour_id']['3001']['reservable']
# print("Reservable(2) is %s" % (resv2))

avail = payload['availability_level']

if (resv1 or resv2):
	print("Showing %s availability for date: %s. Reservable bookings might be available" % (avail, START_DATE))

