#!/bin/bash

echo -n "Checking."
while [ 1 ] ; do
	echo -n "."

	py check_availability.py | grep "Reservable bookings might be available"
	if [ $? == 0 ]; then
		echo ""
		echo "`date` Reservation found"
		echo "$output"

		osascript -e 'set volume output muted FALSE'
		osascript -e 'set volume output volume 100'
		for j in 1 2 3 4 5 6 7 8 9 10 ; do afplay "/System/Library/Sounds/Glass.aiff"; done
		osascript -e 'set volume output muted TRUE'
		osascript -e 'tell app "System Events" to display dialog "Yosemite reservation found!!"'

	fi;

	sleep 3;

done;
